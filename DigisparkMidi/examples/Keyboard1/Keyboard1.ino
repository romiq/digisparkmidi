// 1-Button (P0) MIDI keyboard
// romiq.kh@gmail.com, 2016

#include <DigiMidi.h>

uint8_t button1 = 0; // pin number for first button
uint8_t value1 = HIGH;

uchar key, lastKey = 0;
uchar keyDidChange = 0;
uchar midiMsg[8];
uchar channel = 0;
uchar iii;
  
void setup() {
  pinMode(button1, INPUT);
  digitalWrite(button1, HIGH); // pull-up
  // put your setup code here, to run once:
  MidiUSB.begin(); 
}

void loop() {
  // put your main code here, to run repeatedly:
    usbPoll();


    if (usbInterruptIsReady()) {
      value1 = digitalRead(button1);
      if (value1 == LOW) {
        key = 60;
      } else {
        key = 0;
      }
      if (lastKey != key)
        keyDidChange = 1;      
      if (keyDidChange) {
        /* use last key and not current key status in order to avoid lost
           changes in key status. */
        // up to two midi events in one midi msg.
        // For description of USB MIDI msg see:
        // http://www.usb.org/developers/devclass_docs/midi10.pdf
        // 4. USB MIDI Event Packets
        iii = 0;
        if (lastKey) {  /* release */
          midiMsg[iii++] = 0x08;
          midiMsg[iii++] = 0x80;
          midiMsg[iii++] = lastKey;
          midiMsg[iii++] = 0x00;
        }
        if (key) {  /* press */
          midiMsg[iii++] = 0x09;
          midiMsg[iii++] = 0x90;
          midiMsg[iii++] = key;
          midiMsg[iii++] = 0x7f;
        }
        MidiUSB.sendMidi(midiMsg, iii);
        keyDidChange = 0;
        lastKey = key;
      }
      
    }

  
}
