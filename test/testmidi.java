// romiq.kh@gmail.com, 2016

// see also http://www.jsresources.org/examples/MidiInDump.html

import javax.sound.midi.*;

class testmidi {

    static void listDevices() {
        // enum devices
        MidiDevice.Info[] infoList = MidiSystem.getMidiDeviceInfo();
        MidiDevice device;
        for (int i = 0; i < infoList.length; i++) {
            System.out.printf("%d) %s|%s|%s\n", i + 1, infoList[i].getName(), infoList[i].getVendor(), infoList[i].getDescription());
            try {
                device = MidiSystem.getMidiDevice(infoList[i]);
                System.out.printf("  type: %s\n  ", device.getClass().getName());
                //device.open();
                if (device.getMaxTransmitters() != 0)
                    System.out.printf("[IN] ");
                if (device.getMaxReceivers() != 0)
                    System.out.printf("[OUT] ");
                System.out.printf("\n");
                //System.out.printf("  transmitters: %s\n", device.getMaxTransmitters());
                //System.out.printf("  receivers: %d\n", device.getMaxReceivers());
                //device.close();
            } catch (MidiUnavailableException e) {}
            
        }
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    
    static void showMidi(int num) {
        MidiDevice.Info[] infoList = MidiSystem.getMidiDeviceInfo();
        MidiDevice device = null;
        try {
            for (int i = 0; i < infoList.length; i++) {
                if (i + 1 == num) {
                    device = MidiSystem.getMidiDevice(infoList[i]);
                }
            }
            // TODO: if device is null
            device.open();
            Transmitter tr = device.getTransmitter();
            tr.setReceiver(new Receiver() {
                public void close() {
                    System.out.printf("MIDI closed\n");
                }
                public void send(MidiMessage message, long timeStamp) {
                    System.out.printf("MIDI: %d, %s\n", message.getStatus(), message);
                    byte[] data = message.getMessage();
                    System.out.println(bytesToHex(data));
                }
                
            });
            try {
                System.out.printf("Wait for midi, press [ENTER] to stop\n");
                System.in.read();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
            tr.close();
            device.close();
        } catch (MidiUnavailableException e) {
            System.out.println(e.toString());
        }
    }
    
    public static void main(String[] args) {
        if (args.length < 1)
            testmidi.listDevices();
        else
            testmidi.showMidi(Integer.parseInt(args[0]));
    }
}
